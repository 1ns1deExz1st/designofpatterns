package ru.denis.Adapter;

import ru.denis.Adapter.adapter.PrinterAdapter;

import java.util.ArrayList;
import java.util.List;

public class Client {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Молчаливая рыба Кристофер");
        list.add("Следит за тобой");
        list.add("Не облажайся!");
        PrinterAdapter printerAdapter = new PrinterAdapter();
        printerAdapter.print(list);

    }
}
